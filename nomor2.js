function checkTypeNumber(givenNumber) {
    if (typeof givenNumber === "number") {
        if (givenNumber % 2 === 0) {
            console.log('Genap')
        } else {
            console.log('Ganjil')
        }
    } else if (givenNumber == null) {
        console.log("error : where is the param!")
    } else {
        console.log("error : invalid data type")
    }

    return givenNumber
}


console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber('3'))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())