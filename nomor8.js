        const dataPenjualanNovel = [
        {
            idProduct: "BOOK002421",
            namaProduk: "Pulang - Pergi",
            penulis: "Tere Liye",
            hargaBeli: 60000,
            hargaJual: 86000,
            totalTerjual: 150,
            sisaStok: 17,
        },
        {
            idProduct: "BOOK002351",
            namaProduk: "Selamat Tinggal",
            penulis: "Tere Liye",
            hargaBeli: 75000,
            hargaJual: 103000,
            totalTerjual: 171,
            sisaStok: 20,
        },
        {
            idProduct: "BOOK002941",
            namaProduk: "Garis Waktu",
            penulis: "Fiersa Besari",
            hargaBeli: 67000,
            hargaJual: 99000,
            totalTerjual: 213,
            sisaStok: 5,
        },
        {
            idProduct: "BOOK002941",
            namaProduk: "Laskar Pelangi",
            penulis: "Andrea Hirata",
            hargaBeli: 55000,
            hargaJual: 68000,
            totalTerjual: 20,
            sisaStok: 56,
        },
    ];
    getInfoPenjualanNovel(dataPenjualanNovel);

    function getInfoPenjualanNovel(dataPenjualanNovel){
        let totalKeuntungan = 0,
        totalModal = 0,
        persentaseKeuntungan = 0,
        produkBukuTerlaris = {},
        penulisTerlaris = {};
        dataPenjualanNovel.map((data, index) => {
            // total keuntungan
            totalKeuntungan += data.hargaJual - data.hargaBeli;
            // Total modal
            totalModal += data.hargaBeli * data.totalTerjual;
            // Persentase keuntungan
            persentaseKeuntungan += (data.hargaJual - data.hargaBeli) / data.hargaBeli * 100;
            // Produk Buku Terlaris
            produkBukuTerlaris[data.namaProduk] = (produkBukuTerlaris[data.namaProduk] || 0) + 1;
            // Penulis Terlaris
            penulisTerlaris[data.penulis] = (penulisTerlaris[data.penulis] || 0) + 1;
        })
        penulisTerlaris = Object.keys(penulisTerlaris).reduce(function(a, b){ return penulisTerlaris[a] > penulisTerlaris[b] ? a : b });
        produkBukuTerlaris = Object.keys(produkBukuTerlaris).reduce(function(a, b){ return produkBukuTerlaris[a] > produkBukuTerlaris[b] ? a : b });
        let result = {
            'totalKeuntungan' : toRupiah(totalKeuntungan),
            'totalModal': toRupiah(totalModal),
            'persentaseKuntungan': persentaseKeuntungan.toFixed(2) + '%',
            'produkBukuTerlaris': produkBukuTerlaris,
            'penulisTerlaris': penulisTerlaris,
        }
        console.log(result)
        return result
    }


    function toRupiah(bilangan){
        var	reverse     = bilangan.toString().split('').reverse().join(''),
            ribuan 	    = reverse.match(/\d{1,3}/g);
            ribuan	    = ribuan.join('.').split('').reverse().join('');
        return `Rp ${isMinus(bilangan)}${ribuan}`
    }

    function isMinus(bilangan) {
        if (bilangan < 0) {
            return '- '
        }
        return ''
    }    